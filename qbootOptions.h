/*  qbootOptions.h

    Stamatios Sotiropoulos - FMRIB Image Analysis Group

    Copyright (C) 2010 University of Oxford  */

/*  CCOPYRIGHT  */

#if !defined(qbootOptions_h)
#define qbootOptions_h

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include "utils/options.h"
#include "utils/log.h"
#include "utils/tracer_plus.h"

namespace ODFs {

class qbootOptions {
 public:
  static qbootOptions& getInstance();
  ~qbootOptions() { delete gopt; }

  Utilities::Option<std::string> logdir;
  Utilities::Option<bool> forcedir;
  Utilities::Option<std::string> datafile;
  Utilities::Option<std::string> maskfile;
  Utilities::Option<std::string> bvecsfile;
  Utilities::Option<std::string> bvalsfile;
  Utilities::Option<std::string > qshellsfile;
  Utilities::Option<int> modelnum;
  Utilities::Option<int> lmax;
  Utilities::Option<int> npeaks;
  Utilities::Option<float> peak_threshold;
  Utilities::FmribOption<int> peak_finder;
  Utilities::Option<int> nsamples;
  Utilities::Option<float> lambda;
  Utilities::Option<float> delta;
  Utilities::Option<float> alpha;
  Utilities::Option<int> seed;
  Utilities::Option<bool> gfa;
  Utilities::Option<bool> savecoeff;
  Utilities::Option<bool> savemeancoeff;
  Utilities::Option<bool> verbose;
  Utilities::Option<bool> help;

  //Explanation of possible Output files
  //a) If --savecoeff, then only the samples of the coefficients will be saved and no peaks. This is very fast, but the output files can be huge.
  //b) If --savecoeff --savemeancoeff, then only the mean ODF coefficients (across the samples) will be saved for each voxel and again no peaks. This is very fast and also requires less memory/storage.
  //c) If --savemeancoeff, then the mean ODF coefficients (across the samples) will be saved for each voxel, along with the ODF peaks.
  //d) If none of the above is set, only the ODF peaks are saved.


  void parse_command_line(int argc, char** argv,  Utilities::Log& logger);

 private:
  qbootOptions();
  const qbootOptions& operator=(qbootOptions&);
  qbootOptions(qbootOptions&);

  Utilities::OptionParser options;

  static qbootOptions* gopt;
};

 inline qbootOptions& qbootOptions::getInstance(){
   if(gopt == NULL)
     gopt = new qbootOptions();

   return *gopt;
 }

 inline qbootOptions::qbootOptions() :
   logdir(std::string("--ld,--logdir"), std::string("logdir"),
	 std::string("Output directory (default is logdir)"),
	 false, Utilities::requires_argument),
   forcedir(std::string("--forcedir"),false,std::string("Use the actual directory name given - i.e. don't add + to make a new directory"),false,Utilities::no_argument),
   datafile(std::string("-k,--data"), std::string("data"),
	      std::string("Data file"),
	      true, Utilities::requires_argument),
   maskfile(std::string("-m,--mask"), std::string("nodif_brain_mask"),
	    std::string("Mask file"),
	    true, Utilities::requires_argument),
   bvecsfile(std::string("-r,--bvecs"), std::string("bvecs"),
	     std::string("b vectors file"),
	     true, Utilities::requires_argument),
   bvalsfile(std::string("-b,--bvals"), std::string("bvals"),
	     std::string("b values file"),
	     true, Utilities::requires_argument),
   qshellsfile(std::string("--q"), std::string(""),
   	     std::string("\tFile provided with multi-shell data. Indicates the number of directions for each shell"),
   	     false, Utilities::requires_argument),
   modelnum(std::string("--model"),2,
   	    std::string("\tWhich model to use. 1=Tuch's ODFs, 2=CSA ODFs (default), 3=multi-shell CSA ODFs"),
   	    false,Utilities::requires_argument),
   lmax(std::string("--lmax"),4,
   	    std::string("\tMaximum spherical harmonic oder employed (must be even, default=4)"),
   	    false,Utilities::requires_argument),
   npeaks(std::string("--npeaks"),2,
   	    std::string("Maximum number of ODF peaks to be detected (default 2)"),
   	    false,Utilities::requires_argument),
   peak_threshold(std::string("--thr"),0.4,
   	    std::string("\tMinimum threshold for a local maxima to be considered an ODF peak. Expressed as a fraction of the maximum ODF value (default 0.4)"),
   	    false,Utilities::requires_argument),
   peak_finder(std::string("--pf"),1,
   	    std::string("\tWhich peak finder to use. 1=Discrete, 2=Semi-continuous (can be only used with lmax=4) (default=1)"),
   	    false,Utilities::requires_argument),
   nsamples(std::string("--ns,--nsamples"),50,
	   std::string("Number of bootstrap samples (default is 50)"),
	   false,Utilities::requires_argument),
   lambda(std::string("--lambda"),0,  //use 0.006 for model1
	   std::string("Laplace-Beltrami regularization parameter (default is 0)"),
	   false,Utilities::requires_argument),
   delta(std::string("--delta"),0.01,
	   std::string("\tSignal attenuation regularization parameter for models=2,3 (default is 0.01)"),
	   false,Utilities::requires_argument),
   alpha(std::string("--alpha"),0,
	   std::string("\tLaplacian sharpening parameter for model=1 (default is 0, should be smaller than 1)"),
	   false,Utilities::requires_argument),
   seed(std::string("--seed"),8665904,
	   std::string("\tSeed for pseudo-random number generator"),
           false,Utilities::requires_argument),
   gfa(std::string("--gfa"),false,
	   std::string("\tCompute a generalised FA, using the mean ODF in each voxel"),
           false,Utilities::no_argument),
   savecoeff(std::string("--savecoeff"),false,
	   std::string("Save the ODF coefficients instead of the peaks. WARNING: These can be huge files, please use a few bootstrap samples and a low lmax!"),
           false,Utilities::no_argument),
   savemeancoeff(std::string("--savemeancoeff"),false,
	   std::string("Save the mean ODF coefficients across all samples"),
           false,Utilities::no_argument),
   verbose(std::string("-V,--verbose"), false,
	  std::string("Switch on diagnostic messages"),
	  false, Utilities::no_argument),
   help(std::string("-h,--help"), false,
	std::string("Display this message"),
	false, Utilities::no_argument),

   options("qboot", "qboot --help (for list of options)\n")
   {

     try {
       options.add(logdir);
       options.add(forcedir);
       options.add(datafile);
       options.add(maskfile);
       options.add(bvecsfile);
       options.add(bvalsfile);
       options.add(qshellsfile);
       options.add(modelnum);
       options.add(lmax);
       options.add(npeaks);
       options.add(peak_threshold);
       options.add(peak_finder);
       options.add(nsamples);
       options.add(lambda);
       options.add(delta);
       options.add(alpha);
       options.add(seed);
       options.add(gfa);
       options.add(savecoeff);
       options.add(savemeancoeff);
       options.add(verbose);
       options.add(help);
     }
     catch(Utilities::X_OptionError& e) {
       options.usage();
       std::cerr << std::endl << e.what() << std::endl;
     }
     catch(std::exception &e) {
       std::cerr << e.what() << std::endl;
     }
   }
}
#endif
