include $(FSLCONFDIR)/default.mk

PROJNAME = qboot
XFILES   = qboot
SCRIPTS  = qboot_parallel qboot_postproc.sh qboot_preproc.sh
LIBS     = -lfsl-newimage -lfsl-utils -lfsl-miscmaths \
           -lfsl-NewNifti -lfsl-znz -lfsl-cprob

all: qboot

qboot: qboot.o qbootOptions.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
