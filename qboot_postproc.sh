#!/bin/sh
#   Copyright (C) 2012 University of Oxford
#
#   SHCOPYRIGHT
subjdir=$1

numfib=`${FSLDIR}/bin/imglob ${subjdir}.qboot/diff_slices/data_slice_0000/merged_f*samples* | wc -w | awk '{print $1}'`

fib=1
while [ $fib -le $numfib ]
do
    ${FSLDIR}/bin/fslmerge -z ${subjdir}.qboot/merged_th${fib}samples `${FSLDIR}/bin/imglob ${subjdir}.qboot/diff_slices/data_slice_*/merged_th${fib}samples*`
    ${FSLDIR}/bin/fslmerge -z ${subjdir}.qboot/merged_ph${fib}samples `${FSLDIR}/bin/imglob ${subjdir}.qboot/diff_slices/data_slice_*/merged_ph${fib}samples*`
    ${FSLDIR}/bin/fslmerge -z ${subjdir}.qboot/merged_f${fib}samples  `${FSLDIR}/bin/imglob ${subjdir}.qboot/diff_slices/data_slice_*/merged_f${fib}samples*`
    ${FSLDIR}/bin/fslmerge -z ${subjdir}.qboot/dyads${fib} `${FSLDIR}/bin/imglob ${subjdir}.qboot/diff_slices/data_slice_*/dyads${fib}*`
    ${FSLDIR}/bin/fslmerge -z ${subjdir}.qboot/mean_f${fib}samples `${FSLDIR}/bin/imglob ${subjdir}.qboot/diff_slices/data_slice_*/mean_f${fib}samples*`
    ${FSLDIR}/bin/make_dyadic_vectors ${subjdir}.qboot/merged_th${fib}samples ${subjdir}.qboot/merged_ph${fib}samples ${subjdir}.qboot/nodif_brain_mask ${subjdir}.qboot/dyads${fib}

    fib=$(($fib + 1))
done

if [ `${FSLDIR}/bin/imtest ${subjdir}.qboot/diff_slices/data_slice_0000/meanSHcoeff` -eq 1 ];then
    ${FSLDIR}/bin/fslmerge -z ${subjdir}.qboot/mean_SHcoeff `${FSLDIR}/bin/imglob ${subjdir}.qboot/diff_slices/data_slice_*/meanSHcoeff*`
fi

if [ `${FSLDIR}/bin/imtest ${subjdir}.qboot/diff_slices/data_slice_0000/GFA` -eq 1 ];then
    ${FSLDIR}/bin/fslmerge -z ${subjdir}.qboot/GFA `${FSLDIR}/bin/imglob ${subjdir}.qboot/diff_slices/data_slice_*/GFA*`
fi


echo Removing intermediate files

if [ `${FSLDIR}/bin/imtest ${subjdir}.qboot/merged_th1samples` -eq 1 ];then
  if [ `imtest ${subjdir}.qboot/merged_ph1samples` -eq 1 ];then
    if [ `imtest ${subjdir}.qboot/merged_f1samples` -eq 1 ];then
      rm -rf ${subjdir}.qboot/diff_slices
      rm -f ${subjdir}/data_slice_*
      rm -f ${subjdir}/nodif_brain_mask_slice_*
    fi
  fi
fi

if [ `${FSLDIR}/bin/imtest ${subjdir}.qboot/mean_SHcoeff` -eq 1 ];then
    rm -rf ${subjdir}.qboot/diff_slices
    rm -f ${subjdir}/data_slice_*
    rm -f ${subjdir}/nodif_brain_mask_slice_*
fi

echo Done
