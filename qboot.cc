/*   Copyright (C) 2010 University of Oxford

     Stamatios Sotiropoulos - FMRIB Image Analysis Group   */

/*  CCOPYRIGHT  */


#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include "stdlib.h"

#include "utils/log.h"
#include "utils/tracer_plus.h"
#include "armawrap/newmat.h"

#include "qboot.h"
#include "qbootOptions.h"


using namespace std;
using namespace Utilities;
using namespace NEWMAT;
using namespace ODFs;


////////////////////////////////////////////
//       MAIN
////////////////////////////////////////////

int main(int argc, char *argv[])
{
  try{
    // Setup logging:
    Log& logger = LogSingleton::getInstance();
    qbootOptions& opts = qbootOptions::getInstance();
    opts.parse_command_line(argc,argv,logger);
    srand(qbootOptions::getInstance().seed.value());
    ODF_Volume_Manager vm;
    vm.run_all();
  }

  catch(Exception& e) {
    cerr << endl << e.what() << endl;
  }
  catch(X_OptionError& e) {
    cerr << endl << e.what() << endl;
    }

  return 0;
}
